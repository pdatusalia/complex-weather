package E;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import sierra.AsyncTask;

public class Controller {
    @FXML private Label tempDisplayLabel, cityDisplayLabel, errorLabel;
    @FXML private ImageView day1Image, day2Image, day3Image, day4Image, day5Image;
    ImageView[] forecastImageViews;
    @FXML private Label day1Label, day2Label, day3Label, day4Label, day5Label;
    Label[] forecastLabels;
    @FXML private Label day1Name, day2Name, day3Name, day4Name, day5Name;
    Label[] forecastNames;
    @FXML private Label day1Temp, day2Temp, day3Temp, day4Temp, day5Temp;
    Label[] forecastTemps;
    @FXML private ImageView satelliteRadarImage;
    @FXML private Button tempBtn2, tempBtn;
    @FXML private TextField zipTextField;
    @FXML private ComboBox placeComboBox;
    @FXML private ImageView theme;
    @FXML private AnchorPane anchorPane;
    Weather w;
    List<Map<String, Object>> forecast;
    Map<String, Object> currWeather;

    public Controller() {}

    private @FXML void initialize() {
        forecastImageViews = new ImageView[]{day1Image, day2Image, day3Image, day4Image, day5Image};
        forecastLabels = new Label[]{day1Label, day2Label, day3Label, day4Label, day5Label};
        forecastNames = new Label[]{day1Name, day2Name, day3Name, day4Name, day5Name};
        forecastTemps = new Label[]{day1Temp, day2Temp, day3Temp, day4Temp, day5Temp};

        placeComboBox.getEditor().setOnKeyTyped(this::comboKeyTyped);
        AsyncTask t = new GetData();
        t.execute(":auto");
    }


    //Send search request with input from text field.
    public void pressGoBtn(ActionEvent e)
    {
        errorLabel.setVisible(false);

        // location
        String query = placeComboBox.getEditor().getText();

        AsyncTask t = new GetData();
        t.execute(query);
    }

    public void comboKeyTyped(KeyEvent keyEvent) {
        String q = placeComboBox.getEditor().getText();
        placeComboBox.getItems().clear();
        if (q.length() > 2) {
            AsyncTask gp = new GetPlaces();
            gp.execute(q);
        }
    }

    public void clearCombo(ActionEvent event) {
        placeComboBox.getEditor().clear();
    }

    public void autoBtn(ActionEvent event) {
        AsyncTask t = new GetData();
        t.execute(":auto");
    }

    public void forecastC(ActionEvent e) {
        for (int i = 0; i < 5; i++) {
            forecastTemps[i].setText("" + forecast.get(i).get("avgTempC") + " C");
        }
    }

    public void forecastF(ActionEvent e) {
        for (int i = 0; i < 5; i++) {
            forecastTemps[i].setText("" + forecast.get(i).get("avgTempF") + " F");
        }
    }

    public void pressCBtn(ActionEvent event){
        tempDisplayLabel.setText(""+ currWeather.get("tempC") + " C");
    }

    public void pressFBtn(ActionEvent event){
        tempDisplayLabel.setText(""+ currWeather.get("tempF") + " F");
    }

    private class GetPlaces extends AsyncTask<String, MapBox> {

        @Override
        protected MapBox doInBackground(String s) {
            MapBox m = new MapBox(s);
            m.fetch();
            return m;
        }

        @Override
        protected void onPostExecute(MapBox m) {
            List<Map<String, Object>> places = m.getPlaces();
            for (Map p : places) {
                String name = (String) p.get("name");
                placeComboBox.getItems().add(name);
            }

        }
    }

    private class GetImage extends AsyncTask<Weather, Image> {
        @Override
        protected Image doInBackground(Weather weather) {
            Image image = w.getImage();
            return image;
        }

        @Override
        protected void onPostExecute(Image image) {
            satelliteRadarImage.setImage(image);
        }
    }

    private class GetData extends AsyncTask<String, Weather> {
        @Override
        public Weather doInBackground(String s) {
            w = new Weather(s);
            w.fetch();
            return w;

        }


        @Override
        protected void onPostExecute(Weather w) {

            // maybe instead of bool raise an error and then set error label to e.what()
            Boolean success = w.success();
            if (!success) {
                errorLabel.setText("Error");
                errorLabel.setVisible(true);
                return;
            }
            forecast = w.getForecast();
            currWeather = w.getCurrentWeather();


            //Update GUI Labels
            cityDisplayLabel.setText("" + currWeather.get("city") + ", " + currWeather.get("state"));
            tempDisplayLabel.setText(""+ currWeather.get("tempF") + " F");

            //Image Control
            Image todayImageFromJson = new Image("file:AllImages/StandardImages/images/" + currWeather.get("icon"));

            //Our Forecast for the week containing the weather labels and Icons
            ArrayList<Image> images = new ArrayList<Image>();
            forecastLabels[1].setText("hi");
            for (int x = 0; x < 5; x++)
            {
                Image v = new Image("file:AllImages/StandardImages/images/" + forecast.get(x).get("icon"));
                Date day = new Date((long) forecast.get(x).get("timestamp") * 1000);
                String dayOfWeek = new java.text.SimpleDateFormat("EEEE").format(day);
                String month = new java.text.SimpleDateFormat("MMMM").format(day);
                String date = new java.text.SimpleDateFormat("d").format(day);
                String dateStr = String.format("%s", dayOfWeek);
                forecastNames[x].setText(dateStr);
                forecastLabels[x].setText(""+ forecast.get(x).get("weatherDescription"));
                forecastImageViews[x].setImage(v);
                System.out.println(forecast.get(x));
                System.out.println(forecast.get(x).get("avgTempF"));
                forecastTemps[x].setText("" + forecast.get(x).get("avgTempF") + " F");
                images.add(v);
            }


            AsyncTask i = new GetImage();
            i.execute(w);

            //Set Theme
            //Image t = new Image("file:AllImages/Themes/light.jpg");
            //theme.setImage(t);

        }
    }

}
