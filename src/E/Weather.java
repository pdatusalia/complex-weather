package E;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.scene.image.Image;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Weather {
    private final String location;
    private Image image;
    private JsonObject weatherData;
    private JsonObject forecastData;
    private Boolean success;
    private String clientID, clientSecret, coords;
    public Weather(String location) { this.location = location; }

    public void fetch() {
        String baseBatchUrl = "https://api.aerisapi.com/batch/%s?requests=/observations,/forecasts&client_id=%s&client_secret=%s";

        URL batchUrl;

        clientID = "65LdVSbUGD9zhx3Dh4M0i";
        clientSecret = "xkRcNBDJQyB4sU5vb0Ni5ltFPBjcQGhV3KvF2Vt2";


        try {
            batchUrl = new URL(String.format(baseBatchUrl, URLEncoder.encode(location, StandardCharsets.UTF_8), clientID, clientSecret));

        } catch (MalformedURLException e) {
            e.printStackTrace();
            success = false;
            return;
        }

        InputStream batchStream;

        try {
            batchStream = batchUrl.openStream();

        } catch (IOException e) {
            e.printStackTrace();
            success = false;
            return;
        }

        InputStreamReader batchSR = new InputStreamReader(batchStream);

        BufferedReader batchBR = new BufferedReader(batchSR);

        JsonObject batchData = JsonParser.parseReader(batchBR).getAsJsonObject().get("response").getAsJsonObject();

        weatherData = batchData.get("responses").getAsJsonArray().get(0).getAsJsonObject();
        forecastData = batchData.get("responses").getAsJsonArray().get(1).getAsJsonObject();

        success = weatherData.get("success").getAsBoolean() && forecastData.get("success").getAsBoolean();

        if (success) {
            weatherData = weatherData.get("response").getAsJsonObject();
            forecastData = forecastData.get("response").getAsJsonArray().get(0).getAsJsonObject();
        } else {
            success = false;
        }
    }


    public Map<String, Object> getCurrentWeather() {
        int tempC = weatherData.get("ob").getAsJsonObject().get("tempC").getAsInt();
        int tempF = weatherData.get("ob").getAsJsonObject().get("tempF").getAsInt();
        String weatherFull = weatherData.get("ob").getAsJsonObject().get("weather").getAsString();
        String weatherShort = weatherData.get("ob").getAsJsonObject().get("weatherShort").getAsString();
        String city = weatherData.get("place").getAsJsonObject().get("city").getAsString();
        String state = weatherData.get("place").getAsJsonObject().get("state").getAsString();
        String icon = weatherData.get("ob").getAsJsonObject().get("icon").getAsString();
        JsonObject loc = weatherData.get("loc").getAsJsonObject();
        coords = String.format("%s,%s", loc.get("lat").getAsString(), loc.get("long").getAsString());
        long timestamp = weatherData.get("obTimestamp").getAsLong();

        Map<String, Object> weather = new HashMap<>();

        weather.put("tempC", tempC);
        weather.put("tempF", tempF);
        weather.put("weatherFull", weatherFull);
        weather.put("weatherShort", weatherShort);
        weather.put("city", city);
        weather.put("state", state);
        weather.put("icon", icon);
        weather.put("timestamp", timestamp);
        weather.put("coords", coords);

        return weather;

    }

    public List<Map<String, Object>> getForecast() {
        List<Map<String, Object>> forecast = new ArrayList<>();

        for (JsonElement dayJE : forecastData.get("periods").getAsJsonArray()) {
            JsonObject day = dayJE.getAsJsonObject();

            int maxTempC = day.get("maxTempC").getAsInt();
            int maxTempF = day.get("maxTempF").getAsInt();
            int minTempC = day.get("minTempC").getAsInt();
            int minTempF = day.get("minTempF").getAsInt();
            int avgTempC = day.get("avgTempC").getAsInt();
            int avgTempF = day.get("avgTempF").getAsInt();

            String weatherDescription = day.getAsJsonObject().get("weather").getAsString();
            String icon = day.getAsJsonObject().get("icon").getAsString();
            long timestamp = day.getAsJsonObject().get("timestamp").getAsLong();

            Map<String, Object> weather = new HashMap<>();
            weather.put("maxTempC", maxTempC);
            weather.put("maxTempF", maxTempF);
            weather.put("minTempC", minTempC);
            weather.put("minTempF", minTempF);
            weather.put("avgTempC", avgTempC);
            weather.put("avgTempF", avgTempF);
            weather.put("weatherDescription", weatherDescription);
            weather.put("icon", icon);
            weather.put("timestamp", timestamp);

            forecast.add(weather);
        }
        return forecast;
    }

    public Image getImage() {
        String baseImageUrl =  "https://maps.aerisapi.com/%s_%s/flat,radar,counties,interstates,admin-cities/256x256/%s,8/current.png";
        String imageUrl;
        imageUrl = String.format(baseImageUrl, clientID, clientSecret, coords);
        image = new Image(imageUrl);
        return image;
    }


    public String getCoords() {
        return coords;
    }

    public boolean success() { return success; }
    public static void main(String[] args) {
        Weather w = new Weather("95747");
        w.fetch();

        if (w.success()) {
            Map<String, Object> currWeather = w.getCurrentWeather();

            List<Map<String, Object>> forecast = w.getForecast();

            for (Map<String, Object> day : forecast) {
                // day of week, month, date from timestamp
                Date dateO = new Date((long) day.get("timestamp") * 1000);
                String dayOfWeek = new java.text.SimpleDateFormat("EEEE").format(dateO);
                String month = new java.text.SimpleDateFormat("MMMM").format(dateO);
                String date = new java.text.SimpleDateFormat("d").format(dateO);

                System.out.println(String.format("Date: %s %s %s. Weather: %s", dayOfWeek, month, date, day.get("weatherDescription")));
            }
        }
    }
}
