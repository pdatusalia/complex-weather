package E;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class MapBox {
    private final String input;
    List<Map<String, Object>> places;

    public MapBox(String input) {
        this.input = input;
    }

    public void fetch() {
        String baseUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/%s.json?access_token=%s&limit=10&types=place,postcode&language=en";
        String apiKey = "pk.eyJ1Ijoic2llcnJhY3MiLCJhIjoiY2p2YWExN3NwMGQ3aTQxbzAxZnh6YzloMiJ9.BL6OLkhv0rHoKtMnL_kEkA";
        URL url = null;

        try {
            url = new URL(String.format(baseUrl, URLEncoder.encode(input, StandardCharsets.UTF_8), apiKey));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        InputStream is = null;

        try {
            is = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        JsonObject data = JsonParser.parseReader(br).getAsJsonObject();
        JsonArray features = data.get("features").getAsJsonArray();

        places = new ArrayList<>();

        for (JsonElement feature : features) {
            JsonObject place = feature.getAsJsonObject();
            JsonArray center = place.get("center").getAsJsonArray();
            String coords = center.get(1).getAsString() + "," + center.get(0).getAsString();
            Map<String, Object> placeMap = new HashMap<>();
            placeMap.put("name", place.get("place_name_en").getAsString());
            placeMap.put("coords", coords);

            places.add(placeMap);
        }
    }

    public List<Map<String, Object>> getPlaces() {
        return places;
    }

    public static void main(String[] args) {
        MapBox mb = new MapBox("san diego");
        mb.fetch();
        System.out.println(mb.getPlaces());
    }
}
