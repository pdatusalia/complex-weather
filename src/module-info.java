module Complex.Weather {
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;
    requires asynctask;
    opens E;

}